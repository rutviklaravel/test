<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');


	Route::get('admin/dashboard',function(){
		return view('admin.dashboard');
	});
	
	Route::get('admin/showuser','admin@showusers')->name('showuser');    


Route::get('profile','profile@showprofile')->middleware('auth');

Route::get('update','profile@showform')->middleware('auth')->name('update');

Route::post('update','profile@updateuser');

Auth::routes();

Route::post('task', 'HomeController@create')->name('create')->middleware('auth');

Route::delete('task/{id}','HomeController@delete')->name('delete')->middleware('auth');

Route::get('/task', 'HomeController@index')->name('home')->middleware('auth');
