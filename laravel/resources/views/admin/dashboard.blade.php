@extends('layouts.app')
@section('content')
	<div class="card">
		<div class="card-header">Dashboard</div>
		<div class="card-body">
			<a href="{{ url('admin/showuser') }}" class="btn btn-primary">Manage User</a>
		</div>
	</div>
@endsection