@extends('layouts.app')
@section('content')
	
					<div class="card-header">
						<b>User List</b>
					</div>
					<div class="card-body">
							{{ csrf_field() }}
						<table class="table table-stripped table-responsive">
							<thead>
								<th>Id</th>
								<th>Img</th>
								<th>Name</th>
								<th>Email</th>
								<th>Hobby</th>
								<th>Gender</th>
								<th>DOB</th>
								<th>Address</th>
								<th>Options</th>
							</thead>
							@foreach($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td><img src="../{{ $user->img }}" height="60px"  width="100px" alt="{{ $user->name }}"></td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->hobby }}</td>
									<td>{{ $user->gen }}</td>
									<td>{{ $user->dob }}</td>
									<td>{{ $user->address }}</td>
									<td><a href="{{ url('admin/user/delete/'.$user->id)}}" class="btn btn-danger"><i class="fa fa-btn fa-trash" aria-hidden="true"></i>Delete</a>  <a href="{{ url('admin/user/edit/'.$user->id) }}" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></td>

								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>		
@endsection