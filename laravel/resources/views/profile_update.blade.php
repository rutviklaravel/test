@extends('layouts.app')
@section('content')
	<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
				<div class="card">
					@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

					<div class="card-header">
						User Profile
					</div>
					<div class="card-body">
						<form action="{{ url('update') }}" method="post" class="form form-group" enctype="multipart/form-data">
							{{ csrf_field() }}
						<table class="table table-stripped">
							@foreach($users as $user)
							<tr>
								<th></th>
								<td><img src="{{ $user->img }}" height="200px" width="200px"></td>
							</tr>
							<tr>
								<th></th>
								<td><input type="file" name="img" class="form-control"></td>
							</tr>
							<tr>
								<th>Name :</th>
								<td><input type="text" name="name" value="{{ $user->name }}" class="form-control"></td>
							</tr>
							<tr>
								<th>Email :</th>
								<td><input type="email" name="email" class="form-control" value="{{ $user->email }}"></td>
							</tr>
							<tr>
								<th>Gender :</th>
								<td>Male <input type="radio" name="gen" value="Male">
									Female <input type="radio" name="gen" value="female">  
								</td>
							</tr>
							<tr>
								<th>Hobby :</th>
								<td>
									Reading <input type="checkbox" name="Hobby[]"  value="Reading"><br>
									Writing <input type="checkbox" name="Hobby[]" value="Writing"><br>	
									Coding <input type="checkbox" name=Hobby[] value="Coding">
								</td>
							</tr>
							<tr>
								<th>Dob :</th>
								<td><input type="date" name="dob" value="{{ $user->dob }}" class="form-control"></td>
							</tr>
							<tr>
								<th>Address:</th>
								<td><textarea name="add" id="" cols="30" rows="5" class="form-control" >{{ $user->address }}</textarea></td>
							</tr>
							<tr>
								<th></th>
								<td><button class="btn btn-success" type="submit">Update</button>
								<input type="reset" value="Reset" class="btn btn-danger"></td>
							</tr>
							@endforeach
						</table>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection