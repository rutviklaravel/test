@extends('layouts.app')
@section('content')
	<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
				<div class="card">
					<div class="card-header">
						User Profile
					</div>
					<div class="card-body">
						<table class="table table-stripped">
							@foreach($users as $user)
							<tr>
								<th></th>
								<td><img src="{{ $user->img }}" height="200px" width="200px"></td>
							</tr>
							<tr>
								<th>Name :</th>
								<td>{{ $user->name }}</td>
							</tr>
							<tr>
								<th>Email :</th>
								<td>{{ $user->email }}</td>
							</tr>
							<tr>
								<th>Gender :</th>
								<td>{{ $user->gen }}</td>
							</tr>
							<tr>
								<th>Hobby :</th>
								<td>{{ $user->hobby }}</td>
							</tr>
							<tr>
								<th>Dob :</th>
								<td>{{ $user->dob }}</td>
							</tr>
							<tr>
								<th>Address :</th>
								<td>{{ $user->address }}</td>
							</tr>
							@endforeach
						</table>
					    <a href="{{ route('update') }}" class="btn btn-primary">Update Profile</a>

					    <a href="{{ route('home') }}" class="btn btn-primary">To-Do App</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection