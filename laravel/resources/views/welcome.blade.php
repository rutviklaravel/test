@extends('layouts.app')
@section('content')
        <style>
            html, body {
                height: 100%;
            }   
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
        <div class="container">
            <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                    </div>
                    @endif
            <div class="content">
                <div class="title">Welcome To To-Do App</div>
                <div class="card">
                    @if(Auth::user())
                    <div class="card">
                        <div class="card-header"> TO-DO App</div>
                        <div class="card-body"><a href="{{ url('/task') }}" class="btn btn-primary">TO-Do App</a></div>
                    </div>
                    @else
                       <div class="card">
                        <div class="card-header">Login To Account</div>
                        <div class="card-body"><a href="{{ url('/login') }}" class="btn btn-primary">Login</a></div>
                    </div><br><br>
                    <div class="card">
                        <div class="card-header">Register New Account</div>
                        <div class="card-body"><a href="{{ url('/register') }}" class="btn btn-primary">Register</a></div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
@endsection