<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\task;
use App\User;
use Auth;

class admin extends Controller
{
   public function showusers()
   {
   	$users = User::all();
   	return view('admin.userlist',['users' => $users]);
   }

}
