<?php

namespace App\Http\Controllers;

use App\task;
use App\User;
use Auth;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *        $task = task::findOrfail($id)->delete();

     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::user()->id;
        $tasks = Auth::user()->task->all();
        return view('task',['tasks' => $tasks]);
        
    }

    public function create(request $request)
    {
        $tasks = Auth::user()->task->all();
        $flag=0;
        foreach ($tasks as $value) 
        {
            if($value->name == $request->input('name'))
            {
                echo '<script>
                        alert("Task Already Added..");
                      </script>';
                $flag++;
                return $this->index();
            }
        }
        if(!$flag)
        {
            $task = new task;
            $task->name = $request->name;
            $task->user_id=Auth::user()->id;
            $task->save();      
        }
        return redirect('/task');
    }

    public function delete($id)
    {
        $task = task::find($id);
        
            if(!$task==null)
            {
                $task->delete();
                return $this->index();
            }
    
        return $this->index();
    }

   

}
