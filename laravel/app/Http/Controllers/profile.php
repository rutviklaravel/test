<?php

namespace App\Http\Controllers;

use App\task;
use App\User;
use Auth;
use Illuminate\Http\Request;
class profile extends Controller
{
    
     public function showprofile()
    {
        $id=Auth::user()->id;
        $user = User::where('id','=',$id)->get();
        return view('profile',['users' => $user]);    
    }

    public function showform()
    {	
    	$id = Auth::user()->id;
    	$user = User::where('id','=',$id)->get();
        return view('profile_update',['users' => $user]);  
    }

    public function updateuser(Request $request)
    {
    	$request->validate([
    	'name' => 'required',
	    'email' => 'required|email',
    	'gen' => 'required',
    	'Hobby'=>'required',
    	'dob' => 'required',
    	'add' => 'required|max:25',
		]);

    	$iname='user_'.Auth::user()->id;
		if($request->hasFile('img')) 
		{
	        $image = $request->file('img');
	        $iname = 'user_'.Auth::user()->id.'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('/img');
	        $image->move($destinationPath, $iname);
	        $path = $destinationPath;
       	}

       	$path='img/'.$iname;

       	$user = new User();
       	$user->exists = true;
       	$user->id= Auth::user()->id;
       	$user->name = $request->input('name');
       	$user->email = $request->input('email');
       	$user->gen = $request->input('gen');
       	$user->img = $path;
       	$user->dob = $request->input('dob');
       	$user->address = $request->input('add');
       	$user->hobby = implode(",", $request->input('Hobby'));
       	$user->save();

       	return $this->showprofile();

    }
}
